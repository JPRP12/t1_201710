package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller 
{

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values)
	{
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag)
	{
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag)
	{
		return model.getMax(bag);
	}
	public static int getMin(IntegersBag bag)
	{
		return model.getMin(bag);
	}
	public static double getLastInteger(IntegersBag bag)
	{
		return model.getLastInteger(bag);
	}
	public static int getTotalLength(IntegersBag bag)
	{
		return model.getTotalLenght(bag);
	}
	
	
}
